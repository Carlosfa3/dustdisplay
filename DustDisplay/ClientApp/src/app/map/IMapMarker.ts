
export interface IMapMarker {
  label: string;
  value: number;
  latitude: number;
  longitude: number;
  isOverLimit: boolean;
}
