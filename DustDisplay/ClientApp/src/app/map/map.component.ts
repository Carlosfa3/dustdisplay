import { Component, OnInit, NgModule } from '@angular/core';
import { IMapMarker } from './IMapMarker';
import { DataService } from '../services/dataService';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit {

  lat: number = 51.529585;
  lng: number = -0.137311;

  markers: IMapMarker[];
  dataService: DataService;

  constructor(dataService: DataService) {
    this.dataService = dataService;
  }

  ngOnInit() {
    this.markers = this.dataService.getSevenDaysAverage();
  }
}
