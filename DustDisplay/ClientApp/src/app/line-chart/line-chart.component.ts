import { Component, OnInit, ViewChild } from '@angular/core';
import { ChartDataSets, ChartOptions } from 'chart.js';
import { Color, BaseChartDirective, Label } from 'ng2-charts';
import * as pluginAnnotations from 'chartjs-plugin-annotation';
import { DataService } from '../services/dataService';

@Component({
  selector: 'app-line-chart',
  templateUrl: './line-chart.component.html',
  styleUrls: ['./line-chart.component.css']
})
export class LineChartComponent implements OnInit {

  private dataService: DataService;
  public title = 'Average Dust for June';

  public lineChartData: ChartDataSets[];
  public lineChartLabels: Label[] = ['6/1/2018', '6/2/2018', '6/3/2018', '6/4/2018', '6/5/2018', '6/6/2018', '6/7/2018', '6/8/2018', '6/9/2018', '6/10/2018', '6/11/2018', '6/12/2018', '6/13/2018', '6/14/2018', '6/15/2018', '6/16/2018', '6/17/2018', '6/18/2018', '6/19/2018', '6/20/2018', '6/21/2018', '6/22/2018', '6/23/2018', '6/24/2018', '6/25/2018', '6/26/2018', '6/27/2018', '6/28/2018', '6/29/2018', '6/30/2018'];
  public lineChartOptions: (ChartOptions & { annotation: any }) = {
    responsive: true,
    scales: {
      // We use this empty structure as a placeholder for dynamic theming.
      xAxes: [{}],
      yAxes: [
        {
          id: 'y-axis-0',
          position: 'left',
          ticks: {
            // Include a dollar sign in the ticks
            callback: function (value, index, values) {
              return value+' ug/m3';
            }
          }
        }
      ]
    },
    annotation: {
      annotations: [
        {
          type: 'line',
          mode: 'horizontal',
          scaleID: 'y-axis-0',
          value: '60',
          borderColor: 'red',
          borderWidth: 2,
          label: {
            enabled: true,
            fontColor: 'orange',
            content: '60ug/m3'
          }
        },
      ],
    },
  };
  public lineChartLegend = true;
  public lineChartType = 'line';
  public lineChartPlugins = [pluginAnnotations];

  @ViewChild(BaseChartDirective) chart: BaseChartDirective;

  constructor(dataService: DataService) {
    this.dataService = dataService;
  }

  ngOnInit() {
    this.lineChartData = this.dataService.getPastMonthDustReadings();
  }
}
