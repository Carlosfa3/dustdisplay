import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ChartsModule } from 'ng2-charts';
import { MatButtonModule, MatCheckboxModule, MatTabsModule } from '@angular/material';
import { LineChartComponent } from './line-chart/line-chart.component';
import { MapComponent } from './map/map.component';
import { AgmCoreModule } from '@agm/core';
import { DataService } from './services/dataService';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LineChartComponent,
    MapComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ChartsModule,
    MatButtonModule, MatCheckboxModule, MatTabsModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAdvmbNj3pP8CbZqnSUTA2cAjyjvmab-Ss'
    })
  ],
  providers: [DataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
