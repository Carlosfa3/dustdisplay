import { Injectable } from '@angular/core';
import { ChartDataSets } from 'chart.js';
import { IMapMarker } from '../map/iMapMarker';

@Injectable()
export class DataService {
  constructor() {

  }

  public getPastMonthDustReadings() {
    let dataSet: ChartDataSets[] = [
      {
        data: [45.44, 31.39, 23.80, 62.22, 29.08, 46.12, 41.47, 38.92, 38.76, 41.31, 24.45, 27.62, 21.36, 29.98, 28.13, 20.07, 18.21, 25.17, 14.70, 31.64, 14.88, 20.36, 24.10, 31.89, 25.45, 28.87, 31.13, 29.60, 17.37, 17.21], label: 'Dust_PM10_WW3'
      },
      {
        data: [36.62, 27.14, 19.31, 30.86, 19.01, 12.41, 8.69, 8.50, 12.10, 15.06, 6.64, 6.40, 4.87, 7.51, 5.16, 4.68, 7.75, 10.44, 9.58, 11.87, 7.72, 3.59, 6.59, 11.45, 9.97, 13.34, 12.68, 12.39, 9.99, 10.16], label: 'Dust_PM10_WW2'
      },
      {
        data: [35.88, 25.42, 20.74, 67.53, 37.92, 50.54, 48.42, 44.51, 43.91, 48.43, 31.73, 34.58, 23.52, 46.95, 54.70, 48.70, 52.41, 53.27, 43.38, 48.48, 43.57, 47.35, 51.28, 64.70, 52.98, 60.89, 65.54, 50.47, 30.04, 33.96], label: 'Dust_PM10_A1'
      },
      {
        data: [42.33, 31.25, 22.93, 57.74, 29.74, 39.93, 38.68, 42.92, 36.40, 38.78, 25.96, 29.81, 21.82, 27.15, 25.81, 17.85, 19.98, 21.92, 15.97, 19.52, 19.27, 23.09, 25.11, 31.06, 20.78, 23.76, 29.39, 33.59, 21.81, 23.01], label: 'Dust_PM10_A3'
      },
      {
        data: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0], label: 'Dust_PM10_D1'
      },
      {
        data: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0], label:'Dust_PM10_B1'
      },
      {
        data: [22.95, 15.96, 13.08, 42.62, 19.84, 35.98, 32.72, 29.48, 28.47, 30.38, 18.44, 19.41, 11.28, 17.29, 18.38, 10.30, 10.85, 14.63, 8.17, 10.11, 10.83, 16.09, 18.94, 30.08, 18.41, 20.28, 24.07, 29.10, 21.89, 21.81], label:'Dust_PM10_R1'
      },
      {
        data: [2.68, 4.92, 13.51, 8.41, 6.98, 4.09, 7.42, 7.03, 7.88, 7.21, 10.37, 10.81, 13.04, 11.66, 13.20, 19.24, 17.87, 16.07, 17.90, 14.15, 10.72, 16.04, 21.64, 19.88, 23.67, 22.57, 17.09, 16.01, 12.98, 10.64], label:'Dust_PM10_WW1'
      }
    ];
    return dataSet;
  }

  public getSevenDaysAverage() {
    let mapMarkers: IMapMarker[] = [
      {
        label: 'Dust_PM10_WW3',
        value: 25.93,
        latitude: 51.531581,
        longitude: -0.13862,
        isOverLimit: false
      },
      {
        label: 'Dust_PM10_WW2',
        value: 11.42,
        latitude: 51.529585,
        longitude: -0.137311,
        isOverLimit: false
      },
      {
        label: 'Dust_PM10_A1',
        value: 51.22,
        latitude: 51.530563,
        longitude: -0.134565,
        isOverLimit: true
      },
      {
        label: 'Dust_PM10_A3',
        value: 26.2,
        latitude: 51.531097,
        longitude: -0.13596,
        isOverLimit: false
      },
      {
        label: 'Dust_PM10_D1',
        value: 0.0,
        latitude: 51.532843,
        longitude: -0.140271,
        isOverLimit: false
      },
      {
        label: 'Dust_PM10_B1',
        value: 0.0,
        latitude: 51.53092,
        longitude: -0.138556,
        isOverLimit: false
      },
      {
        label: 'Dust_PM10_R1',
        value: 23.66,
        latitude: 51.532636,
        longitude: -0.141827,
        isOverLimit: false
      },
      {
        label: 'Dust_PM10_WW1',
        value: 17.54,
        latitude: 51.531548,
        longitude: -0.139767,
        isOverLimit: false
      }
    ];
    return mapMarkers;
  }
}
